#!/bin/sh
#
#  tiny_ca sign module
#  (sign a certificate request)
#
#  Copyright 2005-2023, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU General Public
#  License (version 2). You may redistribute and/or modify this program under 
#  the terms of that license as published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  To get a copy of the GNU General Puplic License, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

if [ -z "${1}" ] ; then
    echo "usage: ${0} dirname request [usage]"
    exit -1
fi

if [ ! -f "./${1}/serial" ] ; then
    echo "error: serial file not found."
    exit -2
fi

if [ ! -f "${2}" ] ; then
    echo "error: request file not found."
    exit -4
fi

if [ -n "${3}" ] ; then
    EXTENSION="-extensions x509_extensions_${3}" 
else
    echo "warning: using default x509 extension."
fi

SERIAL=`cat ${1}/serial`

cp "${2}" ${1}/requests/${SERIAL}.pem

[ ${?} -eq 0 ] || exit -8

openssl ca \
    -config   ${1}/config \
    ${EXTENSION} \
    -in       ${1}/requests/${SERIAL}.pem

[ ${?} -eq 0 ] || exit -8

openssl x509 -text -noout \
    -in       ${1}/certs/${SERIAL}.pem

[ ${?} -eq 0 ] || exit -8


