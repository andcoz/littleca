#!/bin/sh
#
#  tiny_ca gennsign module
#  (generate a private key, a request, sign a certificate and pack them in a p12)
#
#  Copyright 2005-2023, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU General Public
#  License (version 2). You may redistribute and/or modify this program under 
#  the terms of that license as published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  To get a copy of the GNU General Puplic License, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

BASENAME=`basename ${0} .sh`

if [ -z "${1}" ] ; then
    echo "usage: ${BASENAME} dirname [usage]"
    exit -1
fi

if [ ! -f "./${1}/serial" ] ; then
    echo "error: serial file not found."
    exit -2
fi

if [ -n "${2}" ] ; then
    EXTENSION="-extensions x509_extensions_${2}" 
else
    echo "warning: using default x509 extension."
fi

SERIAL=`cat ${1}/serial`

echo [[[ ${BASENAME}: CERTIFICATE NUMBER ${SERIAL} ]]]

openssl genrsa \
    -out      ${1}/private/${SERIAL}.pem \
    1024

[ ${?} -eq 0 ] || exit -4

echo [[[ ${BASENAME}: Generating certificate request ]]]

openssl req \
    -new \
    -days     712 \
    -config   ${1}/config \
    -key      ${1}/private/${SERIAL}.pem \
    -out      ${1}/requests/${SERIAL}.pem

[ ${?} -eq 0 ] || exit -4

openssl req \
    -text \
    -verify \
    -noout \
    -config   ${1}/config \
    -in       ${1}/requests/${SERIAL}.pem 

[ ${?} -eq 0 ] || exit -4

openssl ca \
    -md       sha256 \
    -config   ${1}/config \
    ${EXTENSION} \
    -in       ${1}/requests/${SERIAL}.pem

[ ${?} -eq 0 ] || exit -4

openssl x509 \
    -text \
    -noout \
    -in       ${1}/certs/${SERIAL}.pem

[ ${?} -eq 0 ] || exit -4

openssl pkcs12 \
    -export \
    -out      ${1}/private/${SERIAL}.pfx \
    -inkey    ${1}/private/${SERIAL}.pem \
    -in       ${1}/certs/${SERIAL}.pem \
    -certfile ${1}/certs/cacert.pem

[ ${?} -eq 0 ] || exit -4
