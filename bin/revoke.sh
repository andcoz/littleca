#!/bin/sh
#
#  tiny_ca revoke module
#  (revoke a certificate and generate a CRL)
#
#  Copyright 2005-2023, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU General Public
#  License (version 2). You may redistribute and/or modify this program under 
#  the terms of that license as published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  To get a copy of the GNU General Puplic License, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

if [ -z "${1}" ] ; then
    echo "usage: ${0} dirname serial|certificate reason"
    exit -1
fi

if [ ! -f "./${1}/serial" ] ; then
    echo "error: serial file not found."
    exit -2
fi

if [ -z "${2}" ] ; then
    echo "error: serial number not found."
    exit -4
fi

if [ -z "${3}" ] ; then
    echo "error: reason not found (e.g. unspecified, keyCompromise, CACompromise, affiliationChanged, superseded, cessationOfOperation, certificateHold or removeFromCRL)."
    exit -4
fi

SERIAL=`cat ${1}/serial`
CRLID=`cat ${1}/crlnumber`

CERTFILE=

if [ -f ${2} ] ; then 
  CERTFILE=${2}
elif [ -f ${1}/certs/${2}.pem ] ; then
  CERTFILE=${1}/certs/${2}.pem
fi

if [ -z ${CERTFILE} ] ; then
  echo "error: certificate file not found."
  exit -8
fi

openssl ca \
    -config     ${1}/config \
    -revoke     ${CERTFILE} \
    -crl_reason ${3}

[ ${?} -eq 0 ] || exit -16

openssl ca -gencrl \
    -config     ${1}/config \
    -out        ${1}/crl/${CRLID}.pem

[ ${?} -eq 0 ] || exit -16

openssl crl \
    -text -noout \
    -inform     PEM \
    -in         ${1}/crl/${CRLID}.pem

[ ${?} -eq 0 ] || exit -16
