#!/bin/sh
#
#  tiny_ca seed module
#  (generate the directories structure for a little ca)
#
#  Copyright 2005-2023, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU General Public
#  License (version 2). You may redistribute and/or modify this program under 
#  the terms of that license as published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  To get a copy of the GNU General Puplic License, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

if [ -z "${1}" ] ; then
    echo "usage: ${0} dirname"
    exit -1
fi

mkdir ${1}
mkdir ${1}/certs
mkdir ${1}/private
mkdir ${1}/requests
mkdir ${1}/crl

# echo '01' > ${1}/serial
echo '01' > ${1}/crlnumber
echo -n > ${1}/index.txt

cat <<EOF > ${1}/config 
[ ca ] 
default_ca       = ca_default

[ ca_default ]
dir              = ${PWD}/${1}

database         = \$dir/index.txt
new_certs_dir    = \$dir/certs
crl_dir          = \$dir/crl

serial           = \$dir/serial
crlnumber        = \$dir/crlnumber
certificate      = \$dir/certs/cacert.pem
private_key      = \$dir/private/cakey.pem
crl              = \$dir/crl/crl.pem

RANDFILE         = \$dir/private/.rand

x509_extensions  = x509_extensions_user

default_days     = 365
default_crl_days = 30

default_md       = sha256
preserve         = no

policy           = policy_default
email_in_dn      = yes

name_opt          = ca_default
cert_opt          = ca_default
copy_extensions   = copy

[ req ]
default_bits           = 2048
default_keyfile        = keyfile.pem
distinguished_name     = req_distinguished_name
attributes             = req_attributes
prompt                 = yes
x509_extensions        = x509_extensions_user
req_extensions         = req_extensions

[ req_extensions ]
# subjectAltName         = email:move

[ policy_default ]
countryName            = supplied
stateOrProvinceName    = optional
organizationName       = supplied
organizationalUnitName = supplied
commonName             = supplied
emailAddress           = optional

[ policy_match ]
countryName             = match
stateOrProvinceName     = match
localityName            = supplied
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_anything ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ x509_extensions_user ]
basicConstraints       = CA:FALSE
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid,issuer:always
subjectAltName         = email:move
#nsCertType            = client, email, objsign
keyUsage               = nonRepudiation, digitalSignature, keyEncipherment
#crlDistributionPoints = URI:https://littleca.example.com/littleca.crl

[ x509_extensions_user_has_san ]
basicConstraints       = CA:FALSE
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid,issuer:always
#nsCertType            = client, email, objsign
keyUsage               = nonRepudiation, digitalSignature, keyEncipherment
#crlDistributionPoints = URI:https://littleca.example.com/littleca.crl

[ x509_extensions_server ]
basicConstraints       = CA:FALSE
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid,issuer:always
subjectAltName         = email:move
#nsCertType            = server
keyUsage               = nonRepudiation, digitalSignature, keyEncipherment
#crlDistributionPoints = URI:https://littleca.example.com/littleca.crl

[ x509_extensions_server_has_san ]
basicConstraints       = CA:FALSE
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid,issuer:always
#nsCertType            = server
keyUsage               = nonRepudiation, digitalSignature, keyEncipherment
#crlDistributionPoints = URI:https://littleca.example.com/littleca.crl

[ x509_extensions_ca ]
basicConstraints       = critical, CA:TRUE, pathlen:0
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid:always,issuer:always
subjectAltName         = email:move
#nsCertType            = critical, sslCA, emailCA, objsign
keyUsage               = critical, cRLSign, keyCertSign
#crlDistributionPoints = URI:https://littleca.example.com/littleca.crl

[ x509_extensions_ca_has_san ]
basicConstraints       = critical, CA:TRUE, pathlen:0
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid:always,issuer:always
#nsCertType            = critical, sslCA, emailCA, objsign
keyUsage               = critical, cRLSign, keyCertSign
#crlDistributionPoints = URI:https://littleca.example.com/littleca.crl

[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = IT
countryName_min                 = 2
countryName_max                 = 2
stateOrProvinceName             = State or Province Name (full name)
localityName                    = Locality Name (eg, city)
0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = Example Labs
organizationalUnitName          = Organizational Unit Name (eg, section)
organizationalUnitName_default  = Example Labs - Intranet Administration
commonName                      = Common Name (eg, YOUR name)
commonName_max                  = 128
emailAddress                    = Email Address
emailAddress_max                = 128

[ req_attributes ]
# Empty but required

EOF

echo Please, customize your configuration file: ${1}/config

