#!/bin/sh
#
#  tiny_ca genca module
#  (generate a private key and certificate for a little ca)
#
#  Copyright 2005-2023, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU General Public
#  License (version 2). You may redistribute and/or modify this program under 
#  the terms of that license as published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  To get a copy of the GNU General Puplic License, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

if [ -z "${1}" ] ; then
    echo "usage: ${0} dirname"
    exit -1
fi

openssl genrsa \
    -out    ${1}/private/cakey.pem 4096

[ ${?} -eq 0 ] || exit -2

openssl req \
    -new \
    -config ${1}/config \
    -key    ${1}/private/cakey.pem \
    -out    ${1}/requests/careq.pem

[ ${?} -eq 0 ] || exit -2

openssl req \
    -text \
    -verify \
    -noout \
    -config ${1}/config \
    -in     ${1}/requests/careq.pem 

[ ${?} -eq 0 ] || exit -2

openssl ca \
    -create_serial \
    -selfsign \
    -md     sha256 \
    -days   712 \
    -extensions x509_extensions_ca \
    -config ${1}/config \
    -in     ${1}/requests/careq.pem \
    -key    ${1}/private/cakey.pem \
    -out    ${1}/certs/cacert.pem

[ ${?} -eq 0 ] || exit -2

openssl x509 \
    -text \
    -noout \
    -in     ${1}/certs/cacert.pem

[ ${?} -eq 0 ] || exit -2

openssl pkcs12 \
    -export \
    -out    ${1}/private/cacert.pfx \
    -inkey  ${1}/private/cakey.pem \
    -in     ${1}/certs/cacert.pem

[ ${?} -eq 0 ] || exit -2
