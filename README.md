
# LittleCA

Copyright 2005-2023, Andrea Cozzolino <andcoz@gmail.com>

Note that a more extensive version of this documentation,
in Italian, follows the English one.

# Documentation (English Version)

## Purpose

**LittleCA** is a set of simple shell scripts to 
maintain a _basic_ Certification Authority. 

It is designed to help developers to test their 
applications or configurations using certificates
that _resemble_ real ones. It is not designed
to act as a real certification authority.

## Licensing

This program is free software which I release under the GNU 
General Public License (version 2). You may redistribute 
and/or modify this program under the terms of that license 
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.  Version 2 is in the
COPYRIGHT file in the top level directory of this distribution.

Please, note that re-distributing this library under the
version 3 of GNU GPL is explicitly forbidden.

## User Guide

_to be translated_, use italian version

# Documentation (Italian Version)

## Scopo 

**LittleCA** è un set di semplici shell script 
per gestire una _banale_ Certification Authority. 

Il tool è progettato per assistere gli sviluppatori
a testare le proprie configurazioni od applicazioni
usando certificati che _assomiglino_ a quelli reali.
Il tool non è progettato per sostituire una 
vera certification authority.

## Licenza

This program is free software which I release under the GNU 
General Public License (version 2). You may redistribute 
and/or modify this program under the terms of that license 
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.  Version 2 is in the
COPYRIGHT file in the top level directory of this distribution.

Please, note that re-distributing this library under the
version 3 of GNU GPL is explicitly forbidden.

## Guida all'Uso

Questa è una guida minimale all'uso di una ancora più minimale 
libreria di script per gestire una banale certification authority
a scopo di sviluppo o testing di applicazioni crittografiche.

1. Installare openssl (versione >= 0.9.8)

1. Decomprimere il tar del tool

    `tar xvfz littleca.tar.gz`

1. Entrare nella directory appena creata.

    `cd LittleCA`

1. Scegliere il nome simbolico della CA da creare. In questo esempio
   sarà utilizzato il nome `SVILUPPO`.

1. Creare la struttura base della CA con il comando `seed`.

    `bin/seed.sh SVILUPPO`

1. Modificare il file di configurazione appena creato per adattarlo
   alle esigenze del caso. Nella maggioranza dei casi è sufficiente
   modificare i valori di default per il DN della sezione 
   `[ req_distinguished_name ]`.
   
    `emacs SVILUPPO/config`

1. Generare il certificato root della CA con il comando `genca`.

    `bin/genca.sh SVILUPPO`

    Rispondere alle varie domande ed impostare il DN del certificato.
    Impostare la password del pkcs12 che conterrà la chiave privata
    del certificato (in quest caso `SVILUPPO/private/cacert.pfx`).
    Il certificato della CA sarà contenuto, in questo caso, nel file 
    `SVILUPPO/certs/cacert.pem`.

1. Per eseguire in un solo comando tutto il ciclo di generazione
   della chiave, della richiesta e firma del certificato usare
   il comando `gennsign`.
   
    `bin/gennsign.sh SVILUPPO server`

    Notare il secondo parametro del comando che indica il tipo di 
    certificato da emettere. l'utility seed definisce nel file 
    `config` alcune sezioni di base denominate seguendo il seguente
    schema `x509_extensions_NOMETIPOCERTIFICATO` (ad esempio, nel 
    caso del comando immediatamente precedente la sezione utilizzata
    sarà `[x509_extensions_server]`). Di default sono definiti i
    tipi ca, server e user.
   
    La prima riga del output di questo comando è particolarmente
    importante perchè contiene il numero dei certificato in
    emissione (in questo esempio `0a`).
    
    `[[[ gennsign: CERTIFICATE NUMBER 0a ]]]`

    In questo caso, Il certificato firmato sarà registrato nel file 
    `SVILUPPO/certs/0a.pem`. La chiave privata sarà invece registrata
    nel file `SVILUPPO/private/0a.pfx`.

1. I comandi sign e revoke possono essere utilizzati per, rispettivamente,
   firmare richieste di certificazione e revocare un certificato già
   emesso.
